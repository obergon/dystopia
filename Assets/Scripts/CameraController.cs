﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Animator))]
public class CameraController : MonoBehaviour
{
    public CinemachineClearShot PeepCamera;
    public CinemachineClearShot WorldViewCamera;

    public Transform Lake;              // game centre


    private Animator stateCameraAnimator;
    private Peep lookingAtPeep;

    public enum CameraState
    {
        None,
        World,
        Peep
    }
    private CameraState cameraState;


    private void Awake()
    {
        stateCameraAnimator = GetComponent<Animator>();
    }

    private void Start()
    {
        DystopiaEvents.OnWorldView?.Invoke();
    }

    private void OnEnable()
    {
        DystopiaEvents.OnSwitchCamera += OnSwitchCamera;
        DystopiaEvents.OnStatSourceRaycast += OnStatSourceRaycast;
        DystopiaEvents.OnWorldView += OnWorldView;
    }

	private void OnDisable()
    {
        DystopiaEvents.OnSwitchCamera -= OnSwitchCamera;
        DystopiaEvents.OnStatSourceRaycast -= OnStatSourceRaycast;
        DystopiaEvents.OnWorldView -= OnWorldView;
    }


    private void OnSwitchCamera()
    {
        // only one camera will be live...
        PeepCamera.ResetRandomization();
        WorldViewCamera.ResetRandomization();
    }

    // follow and look at peep
    private void OnStatSourceRaycast(StatSource statSource)
    {
        var peep = statSource.GetComponent<Peep>();
        if (peep == null)
            return;

        lookingAtPeep = peep;

		PeepCamera.Follow = lookingAtPeep.transform;
		PeepCamera.LookAt = lookingAtPeep.eyeTransform;      // look the peep in the eye!

        PeepCamera.ResetRandomization();
        ActivatePeepCam();

        DystopiaEvents.OnStatSourceSelected?.Invoke(statSource);
    }

    // world view cameras don't follow
    private void OnWorldView()
    {
        if (lookingAtPeep != null)
        {
            WorldViewCamera.LookAt = lookingAtPeep.eyeTransform;      // look the peep in the eye! (from a distance)
        }
        else if (Lake != null)
        {
            WorldViewCamera.LookAt = Lake;
        }

        WorldViewCamera.ResetRandomization();
        ActivateWorldViewCam();
    }


    private void ActivatePeepCam()
    {
        if (cameraState != CameraState.Peep)
        {
            Debug.Log("ActivatePeepCam");
            cameraState = CameraState.Peep;
            stateCameraAnimator.SetTrigger("PeepCam");

            DystopiaEvents.OnCameraStateChanged?.Invoke(cameraState);
        }
    }

    private void ActivateWorldViewCam()
    {
        if (cameraState != CameraState.World)
        {
            Debug.Log("ActivateWorldViewCam");
            cameraState = CameraState.World;
            stateCameraAnimator.SetTrigger("WorldView");

            DystopiaEvents.OnCameraStateChanged?.Invoke(cameraState);
        }
    }
}
