﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class DaylightChanger : MonoBehaviour
{
	public Light SunLight;  
	public Light MoonLight;

	public float SecondsPerDay = 60f;		// time in seconds for lights to rotate 360 deg (ie. 24 hours)
	public int SunRiseHour = 6;    

	public Color sunMiddayColour;
	public Color sunDuskDawnColour;

	public Color moonMidnightColour;
	public Color moonDuskDawnColour;

	private bool isDaytime;

	private int dayCount;
	private int timeOfDay;

	private bool countingHours;


	private void Start()
	{
		SunLight.color = sunDuskDawnColour;
		MoonLight.color = moonDuskDawnColour;

		SetDaytime(true);
		StartCoroutine(HourCounter());

		// rotate sun and moon around x axis	
		RotateSun();
		RotateMoon();

		CycleSunColours();
		CycleMoonColours();
	}


	private void SetDaytime(bool daytime)
	{
		if (daytime)
		{
			DystopiaEvents.OnSunrise?.Invoke();
			isDaytime = true;
		}
		else
		{
			//Debug.Log("Sunset");
			DystopiaEvents.OnSunset?.Invoke();
			isDaytime = false;
		}
	}


	private IEnumerator HourCounter()
	{
		if (countingHours)
			yield break;

		countingHours = true;

		dayCount = 1;
		timeOfDay = SunRiseHour;
		DystopiaEvents.OnDayCount?.Invoke(dayCount);
		DystopiaEvents.OnHourCount?.Invoke(timeOfDay);

		int dayHourCount = 0;	// 0-23

		while (countingHours)
		{
			// wait 1 'game hour'
			yield return new WaitForSecondsRealtime(SecondsPerDay / 24f);

			dayHourCount++;
			timeOfDay++;

			if (timeOfDay == 24)
			{
				timeOfDay = 0;
			}

			if (dayHourCount >= 24)
			{
				dayCount++;
				dayHourCount = 0;
				DystopiaEvents.OnDayCount?.Invoke(dayCount);
			}

			DystopiaEvents.OnHourCount?.Invoke(timeOfDay);
		}
	}

	private void RotateSun()
	{
		LeanTween.rotateAround(SunLight.gameObject, Vector3.right, 180f, SecondsPerDay / 2f)
				.setEase(LeanTweenType.linear)
				.setOnComplete(() =>
				{
					SetDaytime(!isDaytime);
					RotateSun();
				});
	}

	private void RotateMoon()
	{
		LeanTween.rotateAround(MoonLight.gameObject, Vector3.right, 180f, SecondsPerDay / 2f)
				 .setEase(LeanTweenType.linear)
				.setOnComplete(() =>
				{
					RotateMoon();
				});
	}

	private void CycleSunColours()
	{
		LeanTween.value(gameObject, sunDuskDawnColour, sunMiddayColour, SecondsPerDay / 4f)
				.setEase(LeanTweenType.linear)
				.setOnUpdate((Color val) =>
				{
					SunLight.color = val;
				})
				.setLoopPingPong();
	}

	private void CycleMoonColours()
	{
		LeanTween.value(gameObject, moonDuskDawnColour, moonMidnightColour, SecondsPerDay / 4f)
				.setEase(LeanTweenType.linear)
				.setOnUpdate((Color val) =>
				{
					MoonLight.color = val;
				})
				.setLoopPingPong();
	}
}
