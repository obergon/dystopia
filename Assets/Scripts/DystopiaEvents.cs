﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DystopiaEvents : MonoBehaviour
{
    public delegate void OnGameStartDelegate();
    public static OnGameStartDelegate OnGameStart;

    public delegate void OnSunriseDelegate();
    public static OnSunriseDelegate OnSunrise;

    public delegate void OnSunsetDelegate();
    public static OnSunsetDelegate OnSunset;

    public delegate void OnDayCountDelegate(int dayCount);
    public static OnDayCountDelegate OnDayCount;

    public delegate void OnHourCountDelegate(int hourCount);
    public static OnHourCountDelegate OnHourCount;

    public delegate void OnCameraStateChangedDelegate(CameraController.CameraState state);
    public static OnCameraStateChangedDelegate OnCameraStateChanged;

    public delegate void OnSwitchCameraDelegate();
    public static OnSwitchCameraDelegate OnSwitchCamera;

    public delegate void OnBuildClickedDelegate();
    public static OnBuildClickedDelegate OnBuildClicked;

    public delegate void OnStatSourceRaycastDelegate(StatSource statSouce);
    public static OnStatSourceRaycastDelegate OnStatSourceRaycast;

    public delegate void OnStatSourceSelectedDelegate(StatSource statSouce);
    public static OnStatSourceSelectedDelegate OnStatSourceSelected;

    public delegate void OnToggleStatsUIDelegate(bool visible);
    public static OnToggleStatsUIDelegate OnToggleStatsUI;

    public delegate void OnSourceStatUpdatedDelegate(StatSource statSouce, PeepStat stat);
    public static OnSourceStatUpdatedDelegate OnSourceStatUpdated;

    public delegate void OnWorldViewDelegate();
    public static OnWorldViewDelegate OnWorldView;

    public delegate void OnPeepSeekStateChangedDelegate(Peep peep, Peep.SeekState state);
    public static OnPeepSeekStateChangedDelegate OnPeepSeekStateChanged;

    public delegate void OnCreateStatSourceDelegate(StatsManager.StatSourceType statType, Vector3 position);
    public static OnCreateStatSourceDelegate OnCreateStatSource;

    public delegate void OnSourceMinedDelegate(Peep peep, StatsManager.StatSourceType statType, float quantity);
    public static OnSourceMinedDelegate OnSourceMined;

    public delegate void OnMiningPercChangedDelegate(float rate);
    public static OnMiningPercChangedDelegate OnMiningPercChanged;

    public delegate void OnPlayerGoldUpdatedDelegate(float gold);
    public static OnPlayerGoldUpdatedDelegate OnPlayerGoldUpdated;

    public delegate void OnPlayerGoldZeroDelegate(Player player);
    public static OnPlayerGoldZeroDelegate OnPlayerGoldZero;

    public delegate void OnPeepGoldUpdatedDelegate(Peep peep, float gold, float increment);
    public static OnPeepGoldUpdatedDelegate OnPeepGoldUpdated;

    public delegate void OnPeepStatClickedDelegate(StatsManager.StatSourceType statType);
    public static OnPeepStatClickedDelegate OnPeepStatClicked;


    public delegate void OnPreferenceAddedDelegate(StatSource statSource, PeepStat stat, StatPreference pref);
    public static OnPreferenceAddedDelegate OnPreferenceAdded;

    public delegate void OnPreferenceUpdatedDelegate(StatSource statSource, PeepStat stat, StatPreference pref);
    public static OnPreferenceUpdatedDelegate OnPreferenceUpdated;
}
