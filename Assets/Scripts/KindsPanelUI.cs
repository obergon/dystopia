﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KindsPanelUI : MonoBehaviour
{
    public Image KindsPanel;

    public Text StatName; 
    public StatKindUI KindPrefab;

    public Transform Kinds;     // scroll view content

    private List<StatKindUI> KindsList = new List<StatKindUI>();

    private StatSource selectedStatSource;
    private PeepStat selectedPeepStat;


    private void OnEnable()
    {
        DystopiaEvents.OnStatSourceSelected += OnStatSourceSelected;
        DystopiaEvents.OnPreferenceAdded += OnPreferenceAdded;
        DystopiaEvents.OnPreferenceUpdated += OnPreferenceUpdated;
        DystopiaEvents.OnPeepStatClicked += OnPeepStatClicked;
    }

	private void OnDisable()
    {
        DystopiaEvents.OnStatSourceSelected -= OnStatSourceSelected;
        DystopiaEvents.OnPreferenceAdded -= OnPreferenceAdded;
        DystopiaEvents.OnPreferenceUpdated -= OnPreferenceUpdated;
        DystopiaEvents.OnPeepStatClicked -= OnPeepStatClicked;
    }


    private void OnPeepStatClicked(StatsManager.StatSourceType statType)
    {
        //Debug.Log("KindsPanelUI.OnPeepStatClicked: " + statType);
        if (selectedStatSource != null)
        {
            KindsPanel.gameObject.SetActive(true);

            selectedPeepStat = selectedStatSource.GetStatByType(statType);

            StatName.text = selectedPeepStat.StatName.ToUpper();
            PopulatePreferences(selectedPeepStat);
        }
	}


    private void OnStatSourceSelected(StatSource statSource)
    {
        if (statSource != selectedStatSource)
            KindsPanel.gameObject.SetActive(false);

        selectedStatSource = statSource;
    }


    private void PopulatePreferences(PeepStat peepStat)
    {
        ClearKinds();

        foreach (var kind in peepStat.StatPreferences)
        {
            AddPreferenceUI(kind);
        }
    }

    private StatKindUI GetPreferenceUI(PeepStat peepStat, StatsManager.SourceKind sourceKind)
    {
        foreach (StatKindUI kindUI in KindsList)
        {
            if (kindUI.StatKindPreference.SourceKind == sourceKind)
                return kindUI;
        }

        return null;
    }

    private void AddPreferenceUI(StatPreference statPref)
    {
        var kindUI = Instantiate(KindPrefab, Kinds);

        kindUI.SetPreference(statPref);
        KindsList.Add(kindUI);
    }

    private void ClearKinds()
    {
        foreach (Transform kind in Kinds)
        {
            Destroy(kind.gameObject);
        }
        KindsList.Clear();
    }


	private void OnPreferenceAdded(StatSource statSource, PeepStat stat, StatPreference newPref)
	{
		if (statSource != selectedStatSource)
			return;

        if (selectedPeepStat != null && selectedPeepStat.StatType == newPref.StatType)
		    AddPreferenceUI(newPref);
	}

	private void OnPreferenceUpdated(StatSource statSource, PeepStat stat, StatPreference statPref)
    {
        if (statSource != selectedStatSource)
            return;

        var existingPref = GetPreferenceUI(stat, statPref.SourceKind);

        if (existingPref != null)
            existingPref.SetPreference(statPref);
    }
}
