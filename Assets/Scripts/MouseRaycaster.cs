﻿using UnityEngine;
using UnityEngine.AI;

// physics raycast hit from mouse click (eg. to build)
public class MouseRaycaster : MonoBehaviour
{
    private RaycastHit hitInfo = new RaycastHit();

    public KeyCode FoodKey = KeyCode.F;
    public KeyCode WaterKey = KeyCode.W;
    public KeyCode RestKey = KeyCode.R;
    public KeyCode SocialKey = KeyCode.S;
    public KeyCode GoldKey = KeyCode.G;

    public LayerMask InteractionLayers;         // ground and peeps only


    private void Update()
    {
        if (Input.GetMouseButtonDown(0)) // && !Input.GetKey(KeyCode.LeftShift))
        {
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray.origin, ray.direction, out hitInfo, Mathf.Infinity, InteractionLayers))
            {
                //Debug.Log("Player clicked " + hitInfo.collider.name);

                if (Input.GetKey(FoodKey))
                    DystopiaEvents.OnCreateStatSource?.Invoke(StatsManager.StatSourceType.Food, hitInfo.point);
                else if (Input.GetKey(WaterKey))
                    DystopiaEvents.OnCreateStatSource?.Invoke(StatsManager.StatSourceType.Water, hitInfo.point);
                else if (Input.GetKey(RestKey))
                    DystopiaEvents.OnCreateStatSource?.Invoke(StatsManager.StatSourceType.Rest, hitInfo.point);
                else if (Input.GetKey(SocialKey))
                    DystopiaEvents.OnCreateStatSource?.Invoke(StatsManager.StatSourceType.Social, hitInfo.point);
                else if (Input.GetKey(GoldKey))
                    DystopiaEvents.OnCreateStatSource?.Invoke(StatsManager.StatSourceType.Gold, hitInfo.point);
                else
                {
                    var statSource = hitInfo.transform.gameObject.GetComponent<StatSource>();

                    if (statSource != null)
                        DystopiaEvents.OnStatSourceRaycast?.Invoke(statSource);
                }
            }
        }
    }
}
