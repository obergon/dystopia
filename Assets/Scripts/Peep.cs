﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(StatSource))]
public class Peep : MonoBehaviour
{
	public Animator movementAnimator;

	public float MinSpeed;
	public float MaxSpeed;
	public float TargetFoundDistance = 0.5f;		// stop navigating and turn to look at

	[Space]
	public Collider ContactTrigger;
	public StatSource HomeDwelling;

	[Header("Seeking")]
	public float SeekInterval;		// between raycasts (while seeking)
	public float SeekTimeout;		// returns to waiting if stuck seeking
	private float TurnToRotateTime;
	private float TargetMinDistance = 4f;

	public float reach;
	public float sphereCastRadius;
	public Transform eyeTransform;
	public LayerMask interactionLayers;

	[Header("Mining")]
	public float MiningQuota = 50f;			// each visit to mine
	public float MiningRate = 10f;			// gold per second
	public float GoldMined = 0f;            // running total

	[Tooltip("Percentage chance that next seek target is Gold")]
	[Range(0f, 100f)]
	public float GoldChancePercent = 10f;

	// 'roll the dice' to determine if next target is gold mining
	public bool RollGoldDice { get { return UnityEngine.Random.Range(0, 100) < GoldChancePercent; } }

	private bool turningToTarget = false;		// turn to look at navmesh destination to get raycast hit (to refuel)

	// animation triggers
	private const string idleKickTrigger = "IdleKick"; 
	private const string idleLookAroundTrigger = "IdleLookAround";
	private const string idleScratchHeadTrigger = "IdleScratchHead";
	private const string idleCheckPhoneTrigger = "IdleCheckPhone";
	private const string walkTrigger = "Walk";

	[Space]
	[SerializeField]
	private StatsManager.StatSourceType SeekingStatSource = StatsManager.StatSourceType.None;
	//private StatsManager.SourceKind SeekingSourceKind = StatsManager.SourceKind.None;

	public enum SeekState
	{
		Start,
		Waiting,
		Seeking,
		Refuelling,
		Mining
	}
	[SerializeField]
	public SeekState seekState;

	private StatSource refuellingSource;
	private StatSource lastRefuelledAt;     // to force peeps to move on to another one // TODO: not used
	private PeepStat refuellingStat;

	public float MinWaitTime = 2f;    
	public float MaxWaitTime = 10f;

	private float RandomWaitTime { get { return UnityEngine.Random.Range(MinWaitTime, MaxWaitTime); } }

	//private float mineTime = 6f;        // TODO?


	private NavMeshAgent navMeshAgent;		// required component
	private StatSource peepStatSource;          // required component

	private StatsManager statsManager;          // for source folders // TODO: better way??


	private readonly string[] idleTriggers =
	{
		idleKickTrigger,
		idleLookAroundTrigger,
		idleScratchHeadTrigger,
		idleCheckPhoneTrigger,
	};

	private string RandomIdleTrigger { get { return idleTriggers[UnityEngine.Random.Range(0, idleTriggers.Length)]; } }


	private void Awake()
	{
		navMeshAgent = GetComponent<NavMeshAgent>();
		peepStatSource = GetComponent<StatSource>();
	}

	private void OnEnable()
	{
		DystopiaEvents.OnStatSourceSelected += OnStatSourceSelected;
		DystopiaEvents.OnPeepSeekStateChanged += OnPeepSeekStateChanged;
		DystopiaEvents.OnMiningPercChanged += OnMiningRateChanged;
	}

	private void OnDisable()
	{
		DystopiaEvents.OnStatSourceSelected -= OnStatSourceSelected;
		DystopiaEvents.OnPeepSeekStateChanged -= OnPeepSeekStateChanged;
		DystopiaEvents.OnMiningPercChanged -= OnMiningRateChanged;
	}



	private void Start()
	{
		statsManager = FindObjectOfType<StatsManager>();		// TODO: better way?  SO?
		InitStatsUI();

		// start wait -> seek -> refuel -> consume cycle
		seekState = SeekState.Start;

		PauseForNextSource(RandomWaitTime);
	}


	private StatSource RaycastForTargetSource()
	{
		if (seekState != SeekState.Seeking)
			return null;

		if (SeekingStatSource == StatsManager.StatSourceType.None)
			return null;

		//raycast to find source of target stat source
		var raycastHits = Physics.SphereCastAll(eyeTransform.position,
								sphereCastRadius,
								eyeTransform.forward,
								reach,
								interactionLayers); //,
								//QueryTriggerInteraction.Collide);

		foreach (var hit in raycastHits)
		{
			var hitStatSource = hit.transform.GetComponent<StatSource>();

			// start refuelling if found target source (not self!!)
			if (hitStatSource != null && hitStatSource != peepStatSource && hitStatSource.SourceOf(SeekingStatSource) != null)
			{
				//Debug.Log(name + " Raycast hit - FOUND " + TargetStatSourceType);
				return hitStatSource;
			}
		}
		return null;
	}


	private void OnMiningRateChanged(float rate)
	{
		GoldChancePercent = rate;
	}


	private void OnStatSourceSelected(StatSource statSource)
	{
		var peep = statSource.GetComponent<Peep>();
		if (peep != null)
			peepStatSource.Hilight(peep == this);
	}


	private void OnPeepSeekStateChanged(Peep peep, SeekState state)
	{
		if (peep != this)
			return;

		peepStatSource.StatsUI.SetSourceStatus(SeekStatus(state) + "...");
	}


	public string SeekStatus(SeekState state)
	{
		string statusText;

		if (seekState == SeekState.Seeking)
			statusText = (SeekingStatSource == StatsManager.StatSourceType.Rest && HomeDwelling != null) ? "Home" : SeekingStatSource.ToString();
		else
			statusText = state.ToString();

		return statusText.ToUpper();
	}


	// pause then choose next stat source
	private void PauseForNextSource(float waitTime)
	{
		if (seekState == SeekState.Waiting)
			return;

		//Debug.Log(name + " Wait");

		RandomIdle();
		//ContactTrigger.enabled = true;

		seekState = SeekState.Waiting;
		DystopiaEvents.OnPeepSeekStateChanged?.Invoke(this, seekState);

		LeanTween.value(0f, 1f, waitTime)
						.setEaseLinear()
						.setOnComplete(() => ChooseNextStatSource());
	}

	private void ChooseNextStatSource()
	{
		if (SeekingStatSource != StatsManager.StatSourceType.Gold && RollGoldDice)
		{
			SeekingStatSource = StatsManager.StatSourceType.Gold;
		}
		else
		{
			//Debug.Log(name + " SeekLowestStat currently: " + TargetStatSourceType);
			var lowestStat = peepStatSource.GetLowestStat(SeekingStatSource);

			if (lowestStat != null)
				SeekingStatSource = lowestStat.StatType;
			else
				SeekingStatSource = StatsManager.RandomStatSource(SeekingStatSource);
		}

		StartCoroutine(SeekStatSource());
	}


	private IEnumerator SeekStatSource()
	{
		if (seekState == SeekState.Seeking)
			yield break;

		//Debug.Log(name + " SeekStatSource: " + TargetStatSourceType);

		//Walk();
		//ContactTrigger.enabled = true;

		seekState = SeekState.Seeking;
		DystopiaEvents.OnPeepSeekStateChanged?.Invoke(this, seekState);

		if (SeekingStatSource == StatsManager.StatSourceType.None)
			yield break;

		Transform sourceFolder = statsManager.StatSourceFolder(SeekingStatSource);
		List<StatSource> targetSources = GetTargetStatSources(sourceFolder);

		StatSource targetSource;
		float seekTime = 0f;

		Walk();

		// set a andom speed for navigating to this source
		navMeshAgent.speed = UnityEngine.Random.Range(MinSpeed, MaxSpeed);

		// keep scanning all sources of the target type, since peeps move!
		while (seekState == SeekState.Seeking)
		{
			if (seekTime >= SeekTimeout)
			{
				Debug.Log(name + " Seek Timeout!! " + SeekingStatSource);
				PauseForNextSource(RandomWaitTime);
				yield break;
			}

			// go home (if peep has one)
			if (SeekingStatSource == StatsManager.StatSourceType.Rest && HomeDwelling != null)
				targetSource = HomeDwelling;
			else
				targetSource = FindNearestStatSource(targetSources);        // could change while things are on the move!

			if (targetSource != null)
				NavigateToSource(targetSource);

			//if (!turningToTarget && targetSource != null && navMeshAgent.remainingDistance <= navMeshAgent.stoppingDistance)
			if (!turningToTarget && targetSource != null && navMeshAgent.remainingDistance <= TargetFoundDistance)
			{
				//navMeshAgent.enabled = false;
				navMeshAgent.isStopped = true;
				RandomIdle();

				if (targetSource.SourceCentre != null)
					StartCoroutine(TurnToLookAt(targetSource.SourceCentre.position));
				else
					StartCoroutine(TurnToLookAt(targetSource.CurrentNavTarget.position));
			}

			// keep raycasting for the target source - start refuelling on hit
			var foundStatSource = RaycastForTargetSource();
			if (foundStatSource != null)
			{
				PeepStat fromStat = foundStatSource.GetStatByType(SeekingStatSource);

				if (fromStat != null)
				{
					ReceivePreferences(fromStat, fromStat.IsFakeSource);
					//Wait(RandomWaitTime);

					if (fromStat.IsFakeSource)      // not a true source - just preferences!
					{
						//fromStat.SetZeroTime();		
						PauseForNextSource(RandomWaitTime);
					}
					else                            // genuine source
					{
						RefuelFromTargetSource(foundStatSource, fromStat);      // seekState set to refuelling
						//RandomIdle();

						if (fromStat.IsFiniteSource)
						{
							// source is consumed at the same rate as this peep's refuel rate - slowed by the source's 'exchange rate'
							var exchangeRate = fromStat.ExchangeRate > 0f ? fromStat.ExchangeRate : 1f;
							float peepConsumeRate = peepStatSource.GetStatByType(SeekingStatSource).RefuelRate / exchangeRate;

							foundStatSource.ConsumeFrom(fromStat, OnFuelSourceEmpty, peepConsumeRate);      // stop refuelling when source is empty
						}
					}
					yield break;
				}
			}
			seekTime += SeekInterval;
			yield return new WaitForSeconds(SeekInterval);
		}

		yield return null;
	}


	private void ReceivePreferences(PeepStat fromStat, bool fromFakeSource)
	{
		if (fromStat.StatPreferences.Count == 0)
			return;

		var peepStat = peepStatSource.GetStatByType(SeekingStatSource);

		if (peepStat != null)
		{
			if (fromFakeSource)
				peepStat.TimestampZero();	// effectively goes to end of queue for refuelling // TODO: is this logical? 

			foreach (var fromPref in fromStat.StatPreferences)
			{
				var existingPref = peepStat.GetStatPreference(fromPref.SourceKind);

				if (existingPref != null)      // update this peep's preference
				{
					existingPref.IncrementPreference(fromPref.Preference);
					peepStatSource.StartForgettingPreference(SeekingStatSource, fromPref.SourceKind);		// if not already
					DystopiaEvents.OnPreferenceUpdated?.Invoke(peepStatSource, peepStat, existingPref);
					//Debug.Log(name + " ReceivePreferences from " + fromPref.SourceKind + " EXISTING");
				}
				else        // add new preference to this peep
				{
					var newPref = peepStat.AddStatPreference(fromPref.SourceKind, fromPref.Preference);
					peepStatSource.StartForgettingPreference(SeekingStatSource, fromPref.SourceKind); 
					DystopiaEvents.OnPreferenceAdded?.Invoke(peepStatSource, peepStat, newPref);
					//Debug.Log(name + " ReceivePreferences from " + fromPref.SourceKind + " NEW");
				}
			}
		}
	}

	private IEnumerator TurnToLookAt(Vector3 target)
	{
		float elapsedTime = 0f;

		if (turningToTarget)
			yield break;

		turningToTarget = true;

		Quaternion startRotation = transform.rotation;
		Vector3 targetPosition = new Vector3(target.x, eyeTransform.position.y, target.z);     // same height as peep
		//Vector3 lookDirection = transform.position - targetPosition;
		Vector3 lookDirection = targetPosition - transform.position;

		if (lookDirection == Vector3.zero)
		{
			turningToTarget = false;
			yield break;
		}

		Quaternion targetRotation = Quaternion.LookRotation(lookDirection);

		while (elapsedTime <= TurnToRotateTime)
		{
			transform.rotation = Quaternion.Lerp(startRotation, targetRotation, elapsedTime / TurnToRotateTime);
			elapsedTime += Time.deltaTime;
			yield return null;
		}

		turningToTarget = false;
		yield return null;
	}


	private void RefuelFromTargetSource(StatSource fuelSource, PeepStat fromStat)
	{
		if (seekState == SeekState.Refuelling)
			return;

		if (refuellingSource != null)
			lastRefuelledAt = refuellingSource;

		refuellingSource = fuelSource;
		refuellingStat = fromStat;

		//Debug.Log(name + " Refuel at " + refuelSource.gameObject.name);
		//ContactTrigger.enabled = false;

		seekState = SeekState.Refuelling;
		DystopiaEvents.OnPeepSeekStateChanged?.Invoke(this, seekState);

		// refuelling gold means mining it (for the player)
		if (SeekingStatSource == StatsManager.StatSourceType.Gold)
		{
			//MineForTime(TargetStatSourceType);			// mine for mineTime
			StartCoroutine(MineForQuantity(SeekingStatSource, MiningQuota, MiningRate));	
		}
		else
		{
			peepStatSource.StartRefuelling(SeekingStatSource, StatsManager.SourceKind.None, true, OnStatFull);
			peepStatSource.StopConsumingAll();
			//peepStatSource.StopConsuming(TargetStatSourceType);
		}

		navMeshAgent.velocity = Vector3.zero;
		navMeshAgent.isStopped = true;

		RandomIdle();
	}

	// mining for a fixed time
	//private void MineForTime(StatsManager.StatSourceType statType)
	//{
	//	if (seekState == SeekState.Mining)
	//		return;

	//	RandomIdle();

	//	seekState = SeekState.Mining;
	//	DystopiaEvents.OnPeepSeekStateChanged?.Invoke(this, seekState);

	//	LeanTween.value(0f, 1f, mineTime)
	//					.setEaseLinear()
	//					.setOnUpdate((float qty) => DystopiaEvents.OnSourceMined?.Invoke(statType, qty))
	//					.setOnComplete(() => Wait(RandomWaitTime));
	//}

	// mining for a given quantity
	private IEnumerator MineForQuantity(StatsManager.StatSourceType statType, float miningQuota, float miningRate)
	{
		if (seekState == SeekState.Mining)
			yield break;

		//Debug.Log(name + " MINING " + statType);

		//RandomIdle();       // TODO: needed?

		seekState = SeekState.Mining;
		DystopiaEvents.OnPeepSeekStateChanged?.Invoke(this, seekState);

		float qtyMined = 0;
		float increment = 0;

		while (seekState == SeekState.Mining)
		{
			increment = miningRate * Time.deltaTime;
			qtyMined += increment;			// total this mining session
			GoldMined += increment;         // total for peep

			peepStatSource.StatsUI.UpdateGoldMined(GoldMined);

			DystopiaEvents.OnSourceMined?.Invoke(this, statType, increment);
			DystopiaEvents.OnPeepGoldUpdated?.Invoke(this, GoldMined, increment);

			if (qtyMined >= miningQuota)			// met quota
			{
				//Debug.Log(name + " MINING DONE! qtyMined = " + qtyMined);
				PauseForNextSource(RandomWaitTime);
				yield break;
			}

			yield return null;

			if (seekState != SeekState.Mining)
				Debug.LogError(name + " MINING state changed! " + seekState);
		}

		yield return null;
	}


	private void OnStatFull()
	{
		if (refuellingSource != null)
		{
			//peepStatSource.StartConsuming(TargetStatSourceType, OnStatEmpty);       //TODO: consume all?
			peepStatSource.StartConsumingAll();

			if (refuellingStat != null && refuellingStat.IsFiniteSource)				// stop consuming source when refuel is full
				refuellingStat.StopConsuming();
		}

		//Debug.Log(name + " WAIT - OnStatFull = " + TargetStatSourceType);
		PauseForNextSource(RandomWaitTime);
	}

	private void OnFuelSourceEmpty()
	{
		peepStatSource.StopRefuelling(SeekingStatSource);
		OnStatFull();           // wait and resume consuming all
	}


	//private void OnStatEmpty()
	//{
	//	if (refuelSource != null)
	//	{
	//		peepStatSource.StopConsuming(SeekingStatSource);
	//	}

	//	//Debug.Log(name + " WAIT - OnStatEmpty = " + TargetStatSourceType);
	//	//Wait(waitTime);
	//}

	public void NavigateToSource(StatSource target)
	{
		if (target == null)
			return;

		// get nearest nav target
		var navTarget = target.GetNearestNavTarget(eyeTransform.position);

		if (Vector3.Equals(navMeshAgent.destination, navTarget.position))     // already going there!
			return;

		//navMeshAgent.enabled = true;
		//navMeshAgent.speed = UnityEngine.Random.Range(MinSpeed, MaxSpeed);
		navMeshAgent.isStopped = false;
		navMeshAgent.SetDestination(navTarget.position);
		//navMeshAgent.isStopped = false;

		//Debug.Log(name + " NavigateToSource: " + target.MainStat.StatType + " at " + navTarget.position);
	}

	private void RandomIdle()
	{
		//Debug.Log(name + " Random IDLE");
		movementAnimator.SetTrigger(RandomIdleTrigger);
	}

	private void Walk()
	{
		movementAnimator.SetTrigger(walkTrigger);
	}

	private void InitStatsUI()
	{
		peepStatSource.StatsUI.SetSourceName(peepStatSource.SourceName.ToUpper());
		peepStatSource.StatsUI.UpdateMainStatBar(peepStatSource.MainStat.CurrentValue, false, true);

		for (int i = 0; i < peepStatSource.Stats.Length; i++)
		{
			var peepStat = peepStatSource.Stats[i];

			peepStatSource.StatsUI.SetStatName(i, peepStat.StatName, peepStat.StatType);
			peepStatSource.StatsUI.UpdateStatBar(peepStat.StatType, peepStat.SourceKind, peepStatSource.MainStat.CurrentValue, false, true);
		}
	}


	// search through all sources and return the nearest to the peep
	private StatSource FindNearestStatSource(List<StatSource> targetSources)
	{
		if (seekState != SeekState.Seeking)
			return null;

		StatSource nearestSource = null;
		float nearestDistance = 0f;

		foreach (var statSource in targetSources)
		{
			//if (statSource == lastRefuelledAt)			// forced to seek a diferent source each time
			//	continue;

			float distance = Mathf.Abs(Vector3.Distance(transform.position, statSource.transform.position));

			if (nearestDistance == 0 || distance < nearestDistance)
			{
				nearestSource = statSource;
				nearestDistance = distance;
			}
		}

		return nearestSource;
	}


	private List<StatSource> GetTargetStatSources(Transform sourceFolder)
	{
		if (seekState != SeekState.Seeking)
			return null;

		if (SeekingStatSource == StatsManager.StatSourceType.None)
			return null;

		// lookup peep's preferred kind and 
		StatsManager.SourceKind preferredKind = StatsManager.SourceKind.None;

		if (SeekingStatSource != StatsManager.StatSourceType.Gold)		// gold does not have preferences
		{
			PeepStat seekingStat = peepStatSource.GetStatByType(SeekingStatSource);

			if (seekingStat == null)
			{
				Debug.LogError(name + " GetTargetStatSources: null PeepStat!  seeking: " + SeekingStatSource);
				return null;
			}

			StatPreference preference = seekingStat.GetHighestPreference();

			if (preference != null)
			{
				preferredKind = preference.SourceKind;
				//Debug.Log(name + " GetTargetStatSources " + SeekingStatSource + " preferredKind " + preferredKind);
			}
		}

		//Debug.Log(name + " GetTargetStatSources " + SeekingStatSource + " preferredKind " + preferredKind);

		List<StatSource> sources = new List<StatSource>();

		foreach (Transform source in sourceFolder)
		{
			var statSource = source.GetComponent<StatSource>();
			if (statSource == null)
			{
				Debug.LogError("GetTargetStatSources: " + source.name + " not a StatSource!!");
				continue;
			}

			if (!statSource.gameObject.activeSelf)
				continue;

			// don't consider source currently at
			if (statSource == refuellingSource)
				continue;

			// not a source of this stat / preference
			if (statSource.SourceOf(SeekingStatSource, preferredKind) == null)
				continue;

			//Debug.Log(name + " GetTargetStatSources: found " + statSource.name);

			float distance = Mathf.Abs(Vector3.Distance(transform.position, statSource.transform.position));
			if (distance < TargetMinDistance)       // too close!
			{
				//Debug.Log(name + " target " + source.name + " too close! " + distance);
				continue;
			}

			sources.Add(statSource);
		}

		return sources;
	}


	private void LookAtPhone()
	{
		movementAnimator.SetTrigger(idleCheckPhoneTrigger);
	}

	private void LookAround()
	{
		movementAnimator.SetTrigger(idleLookAroundTrigger);
	}

	private void ScratchHead()
	{
		movementAnimator.SetTrigger(idleScratchHeadTrigger);
	}

	private void Kick()
	{
		movementAnimator.SetTrigger(idleKickTrigger);
	}


	private void OnDrawGizmos()
	{
		if (seekState != SeekState.Seeking)
			return;

		// raycast
		Gizmos.color = Color.white;
		Gizmos.DrawWireSphere(eyeTransform.position, sphereCastRadius);
		Gizmos.DrawWireSphere(eyeTransform.position + eyeTransform.forward * reach, sphereCastRadius);

		// nav mesh destimation
		Gizmos.color = Color.red;
		Gizmos.DrawLine(eyeTransform.position, navMeshAgent.destination);
		Gizmos.DrawWireSphere(navMeshAgent.destination, sphereCastRadius / 2f);
	}
}