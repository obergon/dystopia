﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public class PeepStat
{
	[Header("Stat Details")]
	public StatsManager.StatSourceType StatType;
	public StatsManager.SourceKind SourceKind;

	public string StatName;

	[Range(0, 100f)]
	public float CurrentValue;

	[Tooltip("aggregate of all stats - main stat only")]
	public bool IsAggregateValue = false;		// CurrentValue is aggregate of children

	[Tooltip("% per second - rate at which this stat consumes itself (when not refuelling)")]
	public float ConsumeRate;               // % per second - rate at which this stat is consumed (when not refuelling or consuming?)
	[Tooltip("% per second - rate at which this stat is refuelled (from another source)")]
	public float RefuelRate;                // % per second - rate at which this stat is refuelled

	[Tooltip("source of fuel to others")]
	public bool IsSource;  
	[Tooltip("if finite, consumed while others are refuelling")]
	public bool IsFiniteSource;
	[Tooltip("factor by which this stat is consumed by others")]
	public float ExchangeRate;				// eg. ex. rate of 2 == stat consumed at 0.5 of the peep's refuelling rate

	[Tooltip("true for constantly refuelling stat")]
	public bool AutoRefuel;
	[Tooltip("appear to be a source, but just passes on preferences/influences/beliefs")]
	public bool IsFakeSource;   

	[Header("Source Kind Preferences")]
	public List<StatPreference> StatPreferences = new List<StatPreference>();


	private bool consuming = false;
	private bool refuelling = false;

	public long ZeroTime { get; private set; }			// time (ticks) at which this stat hit zero

	public const float MaxValue = 100f;

	// increase / reduce CurrentValue

	public void StopConsuming()
	{
		consuming = false;
	}

	public void StopRefuelling()
	{
		refuelling = false;
	}

	public IEnumerator Consume(Action<PeepStat> onUpdate, Action onEmpty = null, float overrideConsumeRate = 0f)
	{
		if (consuming)
			yield break;

		if (CurrentValue <= 0)
		{
			StopConsuming();            // TODO: is this correct?
			onEmpty?.Invoke();
			yield break;
		}

		consuming = true;

		// use own ConsumeRate if no override rate specified
		float consumeRate = overrideConsumeRate > 0f ? overrideConsumeRate : ConsumeRate;

		while (consuming)
		{
			CurrentValue -= (consumeRate * Time.deltaTime);

			if (CurrentValue <= 0)
			{
				CurrentValue = 0;
				StopConsuming();            // TODO: is this correct?
				TimestampZero();
				onEmpty?.Invoke();
				yield break;
			}

			onUpdate?.Invoke(this);
			yield return null;
		}

		yield return null;
	}

	public IEnumerator Refuel(bool stopOnFull = true, Action<PeepStat> onUpdate = null, Action onFull = null)
	{
		if (refuelling)
			yield break;

		refuelling = true;
		while (refuelling)
		{
			CurrentValue += (RefuelRate * Time.deltaTime);
			ResetZeroTime();

			if (CurrentValue >= MaxValue)		// full
			{
				CurrentValue = MaxValue;
				onFull?.Invoke();

				if (stopOnFull)
				{
					StopRefuelling();
					yield break;
				}
			}

			onUpdate?.Invoke(this);
			yield return null;
		}

		yield return null;
	}


	public void TimestampZero()
	{
		if (CurrentValue == 0)
			ZeroTime = DateTime.Now.Ticks;
	}

	public void ResetZeroTime()
	{
		ZeroTime = 0;
	}

	// preferences

	public StatPreference AddStatPreference(StatsManager.SourceKind kind, float preference, float qtyCarrying = 0)
	{
		var newPref = new StatPreference { StatType = StatType, SourceKind = kind, Preference = preference, QtyCarrying = qtyCarrying };
		StatPreferences.Add(newPref);
		return newPref;
	}

	public StatPreference GetStatPreference(StatsManager.SourceKind sourceKind)
	{
		foreach (var preference in StatPreferences)
		{
			if (preference.SourceKind == sourceKind)
				return preference;
		}

		return null;
	}

	public StatPreference GetHighestPreference()
	{
		StatPreference highestPref = null;

		foreach (var preference in StatPreferences)
		{
			if (preference.StatType != StatType)
				continue;

			if (preference.SourceKind == StatsManager.SourceKind.None)
				continue;

			if (highestPref == null || preference.Preference > highestPref.Preference)
			{
				highestPref = preference;
			}
		}

		return highestPref;
	}

	public IEnumerator StartForgettingPreference(StatsManager.SourceKind sourceKind, Action<StatPreference> onUpdate, Action onEmpty = null)
	{
		var preference = GetStatPreference(sourceKind);

		if (preference != null)
			yield return preference.StartForgetting(onUpdate, onEmpty);

		yield return null;
	}

	public void StopForgettingPreference(StatsManager.SourceKind sourceKind)
	{
		foreach (var preference in StatPreferences)
		{
			if (preference.SourceKind == sourceKind)
			{
				preference.StopForgetting();
			}
		}
	}
}
