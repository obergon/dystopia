﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PeepStatUI : MonoBehaviour
{
    public Text StatName;
    public Slider StatBar;
    public Button StatButton;

	public StatsManager.StatSourceType StatType { get; private set; }


	private void OnEnable()
	{
		if (StatButton != null)
			StatButton.onClick.AddListener(OnStatClicked);
	}

	private void OnDisable()
	{
		if (StatButton != null)
			StatButton.onClick.RemoveListener(OnStatClicked);
	}

	private void OnStatClicked()
	{
		Debug.Log("PeepStatUI.OnStatClicked " + StatType);
		DystopiaEvents.OnPeepStatClicked?.Invoke(StatType);
	}


	public void Init(StatsManager.StatSourceType statType, string name)
	{
		StatType = statType;
		StatName.text = name;
	}
}