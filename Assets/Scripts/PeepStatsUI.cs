﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Peep stats UIPanel (eg. selected peep)
/// </summary>
public class PeepStatsUI : UIPanel
{
	[Space]
	public GameObject UIPanel;

	public StatsPanelUI StatsPanel;
	public KindsPanelUI KindsPanel;		// for selected stat

	public Button BackButton;			// exit peep cam -> world view


	private void OnEnable()
	{
		BackButton.onClick.AddListener(OnBackClick);
	}

	private void OnDisable()
	{
		BackButton.onClick.RemoveListener(OnBackClick);
	}


	private void OnBackClick()
	{
		DystopiaEvents.OnWorldView?.Invoke();
	}


	public override void Show(bool visible)
	{
		UIPanel.SetActive(visible);

	       //if (visible)
	       //    StatsPanel.InitStatBars();           // sync to saved data
	}
}
