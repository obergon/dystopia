﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
	public float Gold = 0;

	private float GoldConsumeRate = 2f;            // gold units per second - rate at which gold is consumed
	public float goldConsumed = 0;
	private bool consumingGold = false;

	private float goldConsumeFactor = 1f;				// gold consumption goes up as peep happiness goes down

	private void OnEnable()
	{
		DystopiaEvents.OnSourceMined += OnSourceMined;
	}

	private void OnDisable()
	{
		DystopiaEvents.OnSourceMined -= OnSourceMined;
	}

	private void Start()
	{
		DystopiaEvents.OnGameStart?.Invoke();
	}


	private IEnumerator ConsumeGold()
	{
		if (consumingGold)
			yield break;

		if (Gold <= 0)
		{
			DystopiaEvents.OnPlayerGoldZero?.Invoke(this);
			yield break;
		}

		consumingGold = true;

		while (consumingGold)
		{
			float decrement = GoldConsumeRate * goldConsumeFactor * Time.deltaTime;		// TODO: set goldConsumeFactor
			Gold -= decrement;
			goldConsumed += decrement;

			if (Gold <= 0)
			{
				Gold = 0;
				StopConsumingGold();

				DystopiaEvents.OnPlayerGoldUpdated?.Invoke(Gold);
				DystopiaEvents.OnPlayerGoldZero?.Invoke(this);
				yield break;
			}

			DystopiaEvents.OnPlayerGoldUpdated?.Invoke(Gold);
			yield return null;
		}

		yield return null;
	}

	public void StopConsumingGold()
	{
		consumingGold = false;
	}


	// a peep mined some gold
	private void OnSourceMined(Peep peep, StatsManager.StatSourceType statType, float increment)
	{
		if (statType == StatsManager.StatSourceType.Gold)
		{
			Gold += increment;
			DystopiaEvents.OnPlayerGoldUpdated?.Invoke(Gold);

			StartCoroutine(ConsumeGold());		// if not already
		}
	}
}
