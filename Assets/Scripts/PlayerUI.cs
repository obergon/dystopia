﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Player gold, etc
/// </summary>
public class PlayerUI : UIPanel
{
    [Space]
    public GameObject UIPanel;

    public Image GoldPanel;
    public Text GoldCount;
    public Slider MiningPercSlider;
    public Text MiningPercent;      // chance of peeps 'choosing' mining

    [Range(0f, 100f)]
    public float InitMiningRate = 10f;

    public Button BuildButton;
    public Button CameraButton;

    public Toggle StatsToggle;
    public Text StatsToggleText;

    public Text DayCount;
    public Text TimeOfDay;


    private void OnEnable()
    {
        DystopiaEvents.OnPlayerGoldUpdated += OnPlayerGoldUpdated;
        DystopiaEvents.OnDayCount += OnDayCount;
        DystopiaEvents.OnHourCount += OnHourCount;

        BuildButton.onClick.AddListener(OnClickBuild);
        CameraButton.onClick.AddListener(OnClickCamera);

        StatsToggle.onValueChanged.AddListener(OnStatsToggled);
        MiningPercSlider.onValueChanged.AddListener(OnMiningRateChanged);
    }

	private void OnDisable()
    {
        DystopiaEvents.OnPlayerGoldUpdated -= OnPlayerGoldUpdated;
        DystopiaEvents.OnDayCount -= OnDayCount;
        DystopiaEvents.OnHourCount -= OnHourCount;

        BuildButton.onClick.RemoveListener(OnClickBuild);
        CameraButton.onClick.RemoveListener(OnClickCamera);

        StatsToggle.onValueChanged.RemoveListener(OnStatsToggled);
        MiningPercSlider.onValueChanged.RemoveListener(OnMiningRateChanged);
    }

	private void Start()
	{
        MiningPercSlider.value = InitMiningRate;
        MiningPercent.text = (int)InitMiningRate + "%";
        DystopiaEvents.OnMiningPercChanged?.Invoke(InitMiningRate);
    }


	private void OnDayCount(int dayCount)
	{
        DayCount.text = "- Day " + dayCount + " -";
	}

    private void OnHourCount(int hourCount)
    {
        string hour = hourCount < 10 ? ("0" + hourCount).ToString() : hourCount.ToString();
        TimeOfDay.text = hour + ":00";
    }

    private void OnPlayerGoldUpdated(float gold)
    {
        GoldCount.text = ((int)gold).ToString();
    }

    private void OnMiningRateChanged(float rate)
    {
        MiningPercent.text = (int)rate + "%";
        DystopiaEvents.OnMiningPercChanged?.Invoke(rate);
    }

    private void OnStatsToggled(bool visible)
    {
        DystopiaEvents.OnToggleStatsUI?.Invoke(StatsToggle.isOn);
        StatsToggleText.text = visible ? "HIDE\nSTATS" : "SHOW\nSTATS";
    }


    private void OnClickBuild()
	{
        DystopiaEvents.OnBuildClicked?.Invoke();
	}

    private void OnClickCamera()
    {
        DystopiaEvents.OnSwitchCamera?.Invoke();
    }
}
