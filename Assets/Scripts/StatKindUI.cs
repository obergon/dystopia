﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatKindUI : MonoBehaviour
{
    public Text KindName;
    public Text QtyCarrying;

    public Slider PrefSlider;

    public StatPreference StatKindPreference { get; private set; }


	private void OnEnable()
	{
        PrefSlider.onValueChanged.AddListener(OnPrefSliderChanged);
	}

    private void OnDisable()
    {
        PrefSlider.onValueChanged.RemoveListener(OnPrefSliderChanged);
    }


	public void SetPreference(StatPreference statPref)
    {
        StatKindPreference = statPref;

        KindName.text = statPref.DisplayName;
        QtyCarrying.text = statPref.QtyCarrying.ToString();

        PrefSlider.value = statPref.Preference;
    }

    public void UpdatePreference(float preference, float qtyCarrying)
    {
        QtyCarrying.text = qtyCarrying.ToString();
        PrefSlider.value = preference;
    }

    private void OnPrefSliderChanged(float value)
    {
        StatKindPreference.Preference = value;
    }
}
