﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Represents a peep's 'leaning' towards different source (eg. food) types	- eg. prefers HotDog Food over Apple Food
/// Also the type of source that a source has available (for peep refuelling) - eg. Apple Tree
///
/// StatSources will pass on one or more StatPreferences in an exchange
/// </summary>
[Serializable]
public class StatPreference
{
	[Header("Preference Details")]
	public StatsManager.StatSourceType StatType;        // eg. Food
	public StatsManager.SourceKind SourceKind;          // eg. Apple

	public string DisplayedAs;      // uses SourceKind if empty

	public float QtyCarrying;       // quantity available (eg. carrying apples)

	[Range(0, 100f)]
	public float Preference;        // applicable to StatSources that refuel from others

	private const float MaxValue = 100f;

	public string DisplayName { get { return string.IsNullOrEmpty(DisplayedAs) ? SourceKind.ToString().ToUpper() : DisplayedAs.ToUpper(); } }

	[Header("Preference Forgetting")]
	private float ForgetRate = 0.2f;        // % per second - rate at which this preference is 'forgotten'
	private bool forgetting = false;


	public void IncrementPreference(float increment)
	{
		Preference += increment;

		if (Preference < 0)
			Preference = 0;

		if (Preference > MaxValue)
			Preference = MaxValue;
	}

	//TODO: give / take qtyCarrying? reduce over time?

	public IEnumerator StartForgetting(Action<StatPreference> onUpdate, Action onEmpty = null)
	{
		if (forgetting)
			yield break;

		if (Preference <= 0)
			yield break;

		forgetting = true;
		while (forgetting)
		{
			Preference -= (ForgetRate * Time.deltaTime);

			if (Preference <= 0)
			{
				Preference = 0;
				StopForgetting();
				onEmpty?.Invoke();
				yield break;
			}

			onUpdate?.Invoke(this);
			yield return null;
		}

		yield return null;
	}

	public void StopForgetting()
	{
		forgetting = false;
	}
}
