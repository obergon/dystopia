﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Acts as a source for refuelling stats (tree, lake, home, peep)
/// </summary>
public class StatSource : MonoBehaviour
{
	public string SourceName;

	[Header("Stats")]
	public PeepStat MainStat;       // currently represents aggregate of Stats and also the stat consumed by others (eg. food from a tree)
	public PeepStat[] Stats;

	[Header("UI")]
	public StatsPanelUI StatsUI;                // local UI for each StatSource
	public SymbolPanelUI SymbolUI;              // optional (eg. not for trees, lake, etc)
	public HilightPanelUI HilightUI;            // optional (eg. only for peeps)

	[Header("Navigation Targets")]
	public List<Transform> NavTargets;          // targets for navmesh destination setting
	public Transform SourceCentre;

	[Header("Preferences")]
	public List<StatPreference> KindPreferences;      // 'beliefs' exchanged between StatSources - will infuence StatSources chosen to refuel at

	public Transform CurrentNavTarget { get; private set; }

	private float sphereCastRadius = 0.5f;

	//private PeepStat lastLowestStat;        // stat last determined to be lowest - too avoid revisiting same source again


	private void OnEnable()
	{
		DystopiaEvents.OnToggleStatsUI += OnToggleStatsUI;

		StartRefuellingFinite();
	}

	private void OnDisable()
	{
		DystopiaEvents.OnToggleStatsUI -= OnToggleStatsUI;

		StopRefuellingFinite();
	}


	private void Start()
	{
		InitStatUI();
	}


	// finite sources keep refuelling all the time
	private void StartRefuellingFinite()
	{
		if (MainStat.AutoRefuel)
			StartRefuelling(MainStat.StatType, MainStat.SourceKind, false);

		foreach (var stat in Stats)
		{
			if (stat.AutoRefuel)
				StartRefuelling(stat.StatType, stat.SourceKind, false);
		}
	}

	private void StopRefuellingFinite()
	{
		if (MainStat.AutoRefuel)
			StopRefuelling(MainStat.StatType, MainStat.SourceKind);

		foreach (var stat in Stats)
		{
			if (stat.AutoRefuel)
				StopRefuelling(stat.StatType, stat.SourceKind);
		}
	}


	public void Hilight(bool on)
	{
		if (HilightUI != null)
			HilightUI.Hilight(on);
	}

	private void InitStatUI()
	{
		StatsUI.SetMainStatName(MainStat.StatName, MainStat.StatType);
		StatsUI.UpdateMainStatBar(MainStat.CurrentValue, false, true);

		for (int i = 0; i < Stats.Length; i++)
		{
			var stat = Stats[i];
			StatsUI.SetStatName(i, stat.StatName, stat.StatType);
			StatsUI.UpdateStatBar(stat.StatType, stat.SourceKind, stat.CurrentValue, false, true);
		}

		SetStatAggregate();
	}

	private void OnToggleStatsUI(bool visible)
	{
		StatsUI.gameObject.SetActive(visible);
	}

	// peepstats

	public PeepStat GetStatByType(StatsManager.StatSourceType statType, StatsManager.SourceKind sourceKind = StatsManager.SourceKind.None)
	{
		if (MainStat.StatType == statType)
		{
			if (sourceKind == StatsManager.SourceKind.None)
				return MainStat;

			if (MainStat.SourceKind == sourceKind)
				return MainStat;
		}

		foreach (var stat in Stats)
		{
			if (stat.StatType == statType)
			{
				if (sourceKind == StatsManager.SourceKind.None)
					return stat;

				if (stat.SourceKind == sourceKind)
					return stat;
			}
		}

		return null;
	}

	public PeepStat SourceOf(StatsManager.StatSourceType statType, StatsManager.SourceKind sourceKind = StatsManager.SourceKind.None)
	{
		if (statType == StatsManager.StatSourceType.None)
			return null;

		if (MainStat.StatType == statType && (MainStat.IsSource || MainStat.IsFakeSource))
		{
			if (sourceKind == StatsManager.SourceKind.None)
				return MainStat;

			if (MainStat.SourceKind == sourceKind)
				return MainStat;
		}

		foreach (var stat in Stats)
		{
			if (stat.StatType == statType && (stat.IsSource || stat.IsFakeSource))
			{
				if (sourceKind == StatsManager.SourceKind.None)
					return stat;

				if (stat.SourceKind == sourceKind)
					return stat;
			}
		}

		return null;
	}

	// MainStat is only stat that can be aggregate of Stats
	private void SetStatAggregate()
	{
		if (!MainStat.IsAggregateValue)
			return;

		int statCount = 0;
		float totalValue = 0f;

		foreach (var stat in Stats)
		{
			if (stat.StatType != StatsManager.StatSourceType.None)
			{
				statCount++;
				totalValue += stat.CurrentValue;
			}
		}

		float aggregate = (statCount > 0) ? (MainStat.CurrentValue = totalValue / (statCount * PeepStat.MaxValue) * 100f) : 0f;

		MainStat.CurrentValue = aggregate;
		StatsUI.UpdateMainStatBar(MainStat.CurrentValue, false, true);

		DystopiaEvents.OnSourceStatUpdated?.Invoke(this, MainStat);
	}


	// most critical stat to seek next
	public PeepStat GetLowestStat(StatsManager.StatSourceType currentType)
	{
		PeepStat lowestStat = null;
		long lowestZeroTime = 0;			// looking for first to zero

		foreach (var stat in Stats)
		{
			if (stat.StatType == StatsManager.StatSourceType.None)
				continue;

			// ensure not same SourceType as last time...
			if (currentType == stat.StatType)
				continue;

			//if (lastLowestStat != null && stat == lastLowestStat)
			//	continue;

			if (stat.CurrentValue == 0)
			{
				if (lowestZeroTime == 0 || (stat.ZeroTime > 0 && stat.ZeroTime < lowestZeroTime))
				{
					lowestZeroTime = stat.ZeroTime;
					lowestStat = stat;
				}
			}
			else if (lowestStat == null || stat.CurrentValue < lowestStat.CurrentValue)
			{
				lowestStat = stat;
			}
		}

		//lastLowestStat = lowestStat;
		return lowestStat;
	}


	//public void StartConsuming(StatsManager.StatSourceType statType, StatsManager.SourceKind sourceKind = StatsManager.SourceKind.None, Action onFull = null)
	//{
	//	var stat = GetStatByType(statType, sourceKind);

	//	if (stat != null)
	//		StartCoroutine(stat.Consume(OnStatUpdated, onFull));
	//}

	public void StartConsumingAll()
	{
		var food = GetStatByType(StatsManager.StatSourceType.Food);
		var water = GetStatByType(StatsManager.StatSourceType.Water);
		var rest = GetStatByType(StatsManager.StatSourceType.Rest);
		var social = GetStatByType(StatsManager.StatSourceType.Social);

		StartCoroutine(food.Consume(OnStatUpdated));
		StartCoroutine(water.Consume(OnStatUpdated));
		StartCoroutine(rest.Consume(OnStatUpdated));
		StartCoroutine(social.Consume(OnStatUpdated));
	}

	public void StopConsumingAll()
	{
		StopConsuming(StatsManager.StatSourceType.Food);
		StopConsuming(StatsManager.StatSourceType.Water);
		StopConsuming(StatsManager.StatSourceType.Rest);
		StopConsuming(StatsManager.StatSourceType.Social);
	}


	public void StopConsuming(StatsManager.StatSourceType statType, StatsManager.SourceKind sourceKind = StatsManager.SourceKind.None)
	{
		var stat = GetStatByType(statType, sourceKind);

		if (stat != null)
			stat.StopConsuming();
	}

	public void StartRefuelling(StatsManager.StatSourceType statType, StatsManager.SourceKind sourceKind, bool stopOnFull = true, Action onFull = null)
	{
		//Debug.Log(name + " StartRefuelling: " + statType);
		var stat = GetStatByType(statType, sourceKind);

		if (stat != null)
			StartCoroutine(stat.Refuel(stopOnFull, OnStatUpdated, onFull));
		else
			Debug.LogError(name + " StartRefuelling - can't find stat!! (" + stat + ")");
	}

	public void StopRefuelling(StatsManager.StatSourceType statType, StatsManager.SourceKind sourceKind = StatsManager.SourceKind.None)
	{
		//Debug.Log(name + " StopRefuelling");
		var stat = GetStatByType(statType, sourceKind);

		if (stat != null)
			stat.StopRefuelling();
	}


	public void ConsumeFrom(PeepStat fromStat, Action onFuelSourceEmpty, float consumeRate)
	{
		StartCoroutine(fromStat.Consume(OnStatUpdated, onFuelSourceEmpty, consumeRate));       // stop refuelling when source is empty
	}


	// preferences

	public void StartForgettingPreference(StatsManager.StatSourceType statType, StatsManager.SourceKind sourceKind)
	{
		var stat = GetStatByType(statType);

		if (stat != null)
			StartCoroutine(stat.StartForgettingPreference(sourceKind, OnPreferenceUpdated));
	}


	private void OnPreferenceUpdated(StatPreference pref)
	{
		var peepStat = GetStatByType(pref.StatType);
		DystopiaEvents.OnPreferenceUpdated?.Invoke(this, peepStat, pref);
	}


	// navigation

	public Transform GetNearestNavTarget(Vector3 fromPosition)
	{
		float nearestDistance = 0;
		Transform nearestTarget = transform;

		foreach (var target in NavTargets)
		{
			float distance = Mathf.Abs(Vector3.Distance(target.position, fromPosition));

			if (nearestDistance == 0 || distance < nearestDistance)
			{
				nearestTarget = target;
				nearestDistance = distance;
			}
		}

		CurrentNavTarget = nearestTarget;
		return nearestTarget;
	}


	// update UI slider & aggregate
	private void OnStatUpdated(PeepStat stat)
	{
		StatsUI.UpdateStatBar(stat.StatType, stat.SourceKind, stat.CurrentValue, false, true);
		DystopiaEvents.OnSourceStatUpdated?.Invoke(this, stat);

		SetStatAggregate();
	}

	private void OnDrawGizmos()
	{
		Gizmos.color = Color.green;
		foreach (var target in NavTargets)
		{
			Gizmos.DrawWireSphere(target.position, sphereCastRadius);
		}

		if (SourceCentre != null)
		{
			Gizmos.color = Color.yellow;
			Gizmos.DrawWireSphere(SourceCentre.position, sphereCastRadius / 4f);
		}
	}
}
