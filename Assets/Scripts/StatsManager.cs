﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatsManager : MonoBehaviour
{
    public List<StatSource> FoodSourcePrefabs;		// trees
    public List<StatSource> WaterSourcePrefabs;		// lakes
    public List<StatSource> RestSourcePrefabs;		// dwellings
    public List<StatSource> SocialSourcePrefabs;	// peeps
    public List<StatSource> GoldSourcePrefabs;		// gold mines

	//private Dictionary<StatSourceType, List<StatSource>> StatSourcePrefabs;
	//private Dictionary<StatSourceType, List<StatSource>> AvailableSources;

	public GameObject FoodSources;         // folder
	public GameObject WaterSources;            // folder
	public GameObject RestSources;         // folder
	public GameObject SocialSources;         // folder
	public GameObject GoldSources;         // folder

	public enum StatSourceType
	{
		None,
		Food,
		Water,
		Rest,
		Social,
		Happiness,			// 'aggregate' of other states
		Gold				// source for player - peeps work in mine for player
	};

	public enum SourceKind
	{
		None,				// effectively 'n/a'

		// food
		Apple,
		HotDog,

		// water
		Water,
		KoolAid,

		// rest
		Home,
		Caffeine,

		// social
		Peep,
		Phone,
	};


	// ensures not searching for same as current
	public static StatSourceType RandomStatSource(StatSourceType current)
	{
		List<StatSourceType> sources = new List<StatSourceType>();

		if (current != StatSourceType.Food)
			sources.Add(StatSourceType.Food);
		if (current != StatSourceType.Water)
			sources.Add(StatSourceType.Water);
		if (current != StatSourceType.Rest)
			sources.Add(StatSourceType.Rest);
		if (current != StatSourceType.Social)
			sources.Add(StatSourceType.Social);
		if (current != StatSourceType.Gold)
			sources.Add(StatSourceType.Gold);

		return sources[ UnityEngine.Random.Range(0, sources.Count) ];
	}



	private void OnEnable()
	{
		DystopiaEvents.OnCreateStatSource += OnCreateStatSource;
	}

	private void OnDisable()
	{
		DystopiaEvents.OnCreateStatSource -= OnCreateStatSource;
	}


	private void OnCreateStatSource(StatSourceType statType, Vector3 position)
	{
		SpawnStatSource(statType, position);
	}


	private List<StatSource> GetSourcePrefabs(StatSourceType statType)
	{
		switch (statType)
		{
			case StatSourceType.None:
			default:
				return null;

			case StatSourceType.Food:
				return FoodSourcePrefabs;
			case StatSourceType.Water:
				return WaterSourcePrefabs;
			case StatSourceType.Rest:
				return RestSourcePrefabs;
			case StatSourceType.Social:
				return SocialSourcePrefabs;
			case StatSourceType.Gold:
				return GoldSourcePrefabs;
		}
	}

	private void AddAvailableSource(StatSourceType statType, StatSource source)
	{
		List<StatSource> sources = GetSourcePrefabs(statType);      // creates a new key if not present
		sources.Add(source);
	}

	private bool SpawnStatSource(StatSourceType statType, Vector3 position)
	{
		var sourcePrefabs = GetSourcePrefabs(statType);
		if (sourcePrefabs == null || sourcePrefabs.Count == 0)
			return false;

		var sourcePrefab = sourcePrefabs[UnityEngine.Random.Range(0, sourcePrefabs.Count)];
		var newSource = Instantiate(sourcePrefab, StatSourceFolder(statType));
		newSource.transform.position = position;

		AddAvailableSource(statType, newSource);
		return true;
	}

	public Transform StatSourceFolder(StatSourceType statType)
	{
		switch (statType)
		{
			case StatSourceType.None:
			default:
				return transform;

			case StatSourceType.Food:
				return FoodSources.transform;
			case StatSourceType.Water:
				return WaterSources.transform;
			case StatSourceType.Rest:
				return RestSources.transform;
			case StatSourceType.Social:
				return SocialSources.transform;
			case StatSourceType.Gold:
				return GoldSources.transform;
		}
	}

	public float TotalPeepStatValue(StatSourceType statType)
	{
		float statCount = 0;
		float statTotal = 0;

		foreach (Transform peepTrans in SocialSources.transform)
		{
			var peepSource = peepTrans.GetComponent<StatSource>();
			if (peepSource == null)
				continue;

			var peepStat = peepSource.GetStatByType(statType);
			if (peepStat == null)
				continue;

			statTotal += peepStat.CurrentValue;
			statCount++;
		}

		if (statCount == 0)
			return 0;

		return statTotal / (statCount * 100f);		// percent of total stat capacity
	}
}
