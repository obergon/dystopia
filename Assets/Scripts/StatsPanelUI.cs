﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Peep main stat + food / water / rest / social
/// </summary>
public class StatsPanelUI : MonoBehaviour
{
    public Text PeepName;
    public Text PeepStatus;
    public Text GoldMined;

    public bool IsSourceLocalUI;         // ie. over each StatSource

    [Header("Stats")]
    public PeepStatUI MainStatBar;
    public List<PeepStatUI> StatBars = new List<PeepStatUI>();

    public bool ShowStatNames = true;
    public bool FaceCamera = true;

    private float statBarUpdateTime = 0.5f;
    private float statBarPulseScale = 1.25f;
    private float statBarPulseTime = 0.5f;

    private StatSource selectedStatSource;


    private void OnEnable()
    {
        if (!IsSourceLocalUI)
        {
            DystopiaEvents.OnStatSourceSelected += OnStatSourceSelected;
            DystopiaEvents.OnSourceStatUpdated += OnSourceStatUpdated;
            DystopiaEvents.OnPeepSeekStateChanged += OnPeepSeekStateChanged;
            DystopiaEvents.OnPeepGoldUpdated += OnPeepGoldUpdated;
        }

        HideStatNames(!ShowStatNames);
    }

    private void OnDisable()
    {
        if (!IsSourceLocalUI)
        {
            DystopiaEvents.OnStatSourceSelected -= OnStatSourceSelected;
            DystopiaEvents.OnSourceStatUpdated -= OnSourceStatUpdated;
            DystopiaEvents.OnPeepSeekStateChanged -= OnPeepSeekStateChanged;
            DystopiaEvents.OnPeepGoldUpdated -= OnPeepGoldUpdated;
        }
    }


    public void LateUpdate()
    {
        if (FaceCamera)     // make stats UI face camera, but only on the y axis (ie. stay perpendicular to player in world space)
        {
            Vector3 targetPoint = Camera.main.transform.position;

            // project camera position onto xz plane
            targetPoint.y = transform.position.y;

            // Vector3.up is a normal of the xz plane
            //transform.LookAt(targetPoint, Vector3.up);

            // 'like a guard who's been paid not to notice a robbery, we just need to look in the other direction'
            transform.rotation = Quaternion.LookRotation(transform.position - targetPoint);
        }
    }


    private PeepStatUI GetStatUIByType(StatsManager.StatSourceType statType)
    {
        if (MainStatBar.StatType == statType)
            return MainStatBar;

        foreach (var statBar in StatBars)
        {
            if (statBar.StatType == statType)
                return statBar;
        }
        return null;

    }


    private void OnStatSourceSelected(StatSource statSource)
    {
        if (IsSourceLocalUI)            // StatSource local UI updated by each StatSource
            return;

        selectedStatSource = statSource;
        var peep = selectedStatSource.GetComponent<Peep>();

        // set peep stat values, name, status, etc.
        if (peep != null)
        {
            SetSourceName(selectedStatSource.SourceName.ToUpper());
            SetSourceStatus(peep.seekState.ToString().ToUpper() + "...");
            SetSourceGold("MINED: " + (int)peep.GoldMined);

            SetMainStatName(selectedStatSource.MainStat.StatName, selectedStatSource.MainStat.StatType);
            UpdateMainStatBar(selectedStatSource.MainStat.CurrentValue, false, true);

            for (int i = 0; i < selectedStatSource.Stats.Length; i++)
            {
                var peepStat = selectedStatSource.Stats[i];
                SetStatName(i, peepStat.StatName, peepStat.StatType);
                UpdateStatBar(peepStat.StatType, peepStat.SourceKind, peepStat.CurrentValue, false, true);
            }
        }
    }


    private void OnSourceStatUpdated(StatSource statSource, PeepStat peepStat)
    {
        if (IsSourceLocalUI)            // StatSource local UI updated by each StatSource
            return;

        if (statSource == selectedStatSource)
        {
            if (peepStat.StatType == statSource.MainStat.StatType)
                UpdateMainStatBar(selectedStatSource.MainStat.CurrentValue, false, true);
            else
                UpdateStatBar(peepStat.StatType, peepStat.SourceKind, peepStat.CurrentValue, false, true);
        }
    }

    private void OnPeepSeekStateChanged(Peep peep, Peep.SeekState state)
    {
        if (IsSourceLocalUI)            // StatSource local UI updated by each StatSource
            return;

        var selectedPeep = selectedStatSource != null ? selectedStatSource.GetComponent<Peep>() : null;

        if (selectedPeep != null && selectedPeep == peep)
        {
            SetSourceStatus(selectedPeep.SeekStatus(state) + "...");
        }
    }


    // Stats

    public void UpdateStatBar(StatsManager.StatSourceType statType, StatsManager.SourceKind sourceKind, float statValue, bool animate, bool silent)
    {
        var statUI = GetStatUIByType(statType);
        if (statUI != null)
        {
            if (animate)
            {
                bool updated = AnimateStatBar(statUI.StatBar, statValue, silent);

                if (updated && !silent)
                    PulseStatBar(statUI.StatBar);
            }
            else
                statUI.StatBar.value = statValue;
        }
    }

    public void UpdateMainStatBar(float statValue, bool animate, bool silent)
    {
        if (animate)
        {
            bool updated = AnimateStatBar(MainStatBar.StatBar, statValue, silent);

            if (updated && !silent)
                PulseStatBar(MainStatBar.StatBar);
        }
        else
            MainStatBar.StatBar.value = statValue;
    }

    private bool AnimateStatBar(Slider healthBar, float newValue, bool silent = false, float updateTime = 0f)
    {
        if (healthBar.value == newValue)
            return false;

        if (updateTime > 0)
        {
            LeanTween.value(healthBar.gameObject, healthBar.value, newValue, updateTime)
                  .setEaseLinear()
                  .setOnUpdate((float value) =>
                  {
                      healthBar.value = value;
                  });
        }
        else
        {
            LeanTween.value(healthBar.gameObject, healthBar.value, newValue, statBarUpdateTime)
                  .setEaseOutCirc()
                  .setOnUpdate((float value) =>
                  {
                      healthBar.value = value;
                  });
        }

        //if (!silent && newValue != 0f)
        //    PlaySFX(HealthBarAudio);

        return true;
    }

    private void PulseStatBar(Slider healthBar)
    {
        LeanTween.scale(healthBar.gameObject, Vector3.one * statBarPulseScale, statBarPulseTime)
                    .setEase(LeanTweenType.easeInCubic)
                    .setLoopPingPong(1);
    }


    public void HideStatNames(bool hide)
    {
        MainStatBar.StatName.enabled = !hide;

        for (int i = 0; i < StatBars.Count; i++)
        {
            StatBars[i].StatName.enabled = !hide;
        }
    }


    // gold mined

    private void OnPeepGoldUpdated(Peep peep, float gold, float increment)
    {
        if (IsSourceLocalUI)            // StatSource local UI updated by each StatSource
            return;

        var selectedPeep = selectedStatSource != null ? selectedStatSource.GetComponent<Peep>() : null;

        if (selectedPeep != null && selectedPeep == peep)
        {
            GoldMined.text = "MINED: " + ((int)gold).ToString();
        }
    }

    public void UpdateGoldMined(float goldMined)
    {
        GoldMined.text = "MINED: " + ((int)goldMined).ToString();
    }


    public void SetSourceName(string name)
    {
        PeepName.text = name;
    }

    public void SetSourceStatus(string status)
    {
        PeepStatus.text = status;
    }

    public void SetSourceGold(string gold)
    {
        GoldMined.text = gold;
    }


    public void SetMainStatName(string mainStatName, StatsManager.StatSourceType statType)
	{
        MainStatBar.Init(statType, mainStatName);
	}

    public void SetStatName(int index, string statName, StatsManager.StatSourceType statType)
    {
        StatBars[ index ].Init(statType, statName);
    }
}
