﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "PeepStats", menuName = "Peeps/Stats Data")]
public class StatsSO : ScriptableObject
{
    public PeepStat MainStat;
    public PeepStat[] Stats = new PeepStat[ 4 ];       // TODO: variable!


    public void InitStats()
    {
        foreach (var stat in Stats)
        {
            stat.CurrentValue = 0;
        }
    }
}
