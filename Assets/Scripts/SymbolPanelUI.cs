﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SymbolPanelUI : MonoBehaviour
{
    public Text Symbol;
    public bool FaceCamera = true;


    public void LateUpdate()
    {
        if (FaceCamera)     // make stats UI face camera, but only on the y axis (ie. stay perpendicular to player in world space)
        {
            Vector3 targetPoint = Camera.main.transform.position;

            // project camera position onto xz plane
            targetPoint.y = transform.position.y;

            // Vector3.up is a normal of the xz plane
            //transform.LookAt(targetPoint, Vector3.up);

            // 'like a guard who's been paid not to notice a robbery, we just need to look in the other direction'
            transform.rotation = Quaternion.LookRotation(transform.position - targetPoint);
        }
    }

    public void SetSymbol(string symbol)
    {
        Symbol.text = symbol;
    }
}
