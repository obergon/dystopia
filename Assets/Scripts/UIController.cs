﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public UIPanel PlayerStatsPanel;    // gold, etc
    public UIPanel PeepStatsPanel;      // selected peep

    public Image Blackout;
    public Color BlackoutColour;
    public float FadeToBlackTime;


    public AudioSource UIAudioSource;
    public AudioClip FadeInAudio;

    public UIPanel CurrentPanel { get; private set; }


	public void OnEnable()
	{
        DystopiaEvents.OnCameraStateChanged += OnCameraStateChanged;
    }

    public void OnDisable()
    {
        DystopiaEvents.OnCameraStateChanged -= OnCameraStateChanged;
    }

	private void OnCameraStateChanged(CameraController.CameraState state)
	{
		switch (state)
		{
			case CameraController.CameraState.None:
				break;
			case CameraController.CameraState.World:
                PeepStatsPanel.Show(false);
				break;
			case CameraController.CameraState.Peep:
                PeepStatsPanel.Show(true);
                break;
		}
	}


	private void FadeToPanel(UIPanel toPanel, bool force = false, Action OnFaded = null)
    {
        if (!force && CurrentPanel == toPanel)
            return;

        Blackout.color = Color.clear;
        Blackout.gameObject.SetActive(true);

        LeanTween.value(Blackout.gameObject, Color.clear, BlackoutColour, FadeToBlackTime / 2f)
                            .setEase(LeanTweenType.easeOutQuart)
                            .setOnUpdate((Color col) => Blackout.color = col)
                            .setOnComplete(() =>
                            {
                                // switch panels while blacked out
                                //if (hideCurrent && CurrentPanel != null)
                                if (CurrentPanel != null)
                                    CurrentPanel.Show(false);

                                CurrentPanel = toPanel;
                                CurrentPanel.Show(true);

                                PlaySFX(FadeInAudio);

                                OnFaded?.Invoke();

                                // clear blackout
                                LeanTween.value(Blackout.gameObject, BlackoutColour, Color.clear, FadeToBlackTime / 2f)
                                            .setEase(LeanTweenType.easeInQuart)
                                            .setOnUpdate((Color col) => Blackout.color = col)
                                            .setOnComplete(() =>
                                            {
                                                Blackout.gameObject.SetActive(false);
                                            });
                            });
    }

    private void PlaySFX(AudioClip sfx)
    {
        if (sfx == null)
            return;

        UIAudioSource.clip = sfx;
        UIAudioSource.Play();
    }
}
