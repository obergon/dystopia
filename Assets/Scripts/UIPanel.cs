﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPanel : MonoBehaviour
{
    [Header("Menu Audio Sources (Mixer groups)")]
    public AudioSource BGMAudioSource;
    public AudioSource SFXAudioSource;
    public float BGMFadeTime = 0.5f;

    [Header("Menu BGM")]
    public AudioClip MenuBGM;


    public virtual void Show(bool visible)
    {
        gameObject.SetActive(visible);
    }

    protected bool IsBGMPlaying { get { return BGMAudioSource.isPlaying; } }


    protected void PlayBGM(bool fade = false)
    {
        if (MenuBGM == null)
            return;

        BGMAudioSource.clip = MenuBGM;

        if (fade)
            FadeBGMVolume(true);
        else
            BGMAudioSource.Play();
    }

    protected void StopBGM(bool fade = false)
    {
        if (fade)
            FadeBGMVolume(false);
        else
            BGMAudioSource.Stop();
    }

    private void FadeBGMVolume(bool fadeIn)
    {
        if (fadeIn)
        {
            BGMAudioSource.volume = 0;
            BGMAudioSource.Play();
        }

        LeanTween.value((fadeIn ? 0f : 1f), (fadeIn ? 1f : 0f), BGMFadeTime)
                                .setEaseOutQuad()
                                .setOnUpdate((float x) => BGMAudioSource.volume = x)
                                .setOnComplete(() =>
                                {
                                    if (! fadeIn)
                                        BGMAudioSource.Stop();
                                });
    }

    protected void PlaySFX(AudioClip sfx)
    {
        if (sfx == null)
            return;

        SFXAudioSource.clip = sfx;
        SFXAudioSource.Play();
    }
}
